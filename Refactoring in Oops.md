# **Refactoring in Oops**
 Refactoring of Oops is a concept of making code more easy to understand and making code efficient, easy to modify and maintain.


## **What is Refactoring?**

Refactoring is a process of defragmenting code into a small easy understandable code. By doing this we get clean code and will be more efficient/convinient for adding features in future. Refactoring can help undo code entropy. <br/>
 
>Refactoring is a disciplined technique for restructuring an existing body of code, altering its internal structure without changing its external behavior.
<br/>

In simple word, Refactoring is a process of untie the nots to make it simple and readable.
<br/>


To get more information have a look into the [Link](https://youtu.be/-lkiccO8h6w).

## **When to Refactor code?**

Refactoring should be done when you find 
- Code will be Upgraded in future.
- Inorder to reduce duplication of code.
- To find bugs in code.
- When you find code needs to be manupulated to make code more efficient.
- Bad smell in code
  - Long methods - Methods more than 5-10 line and having many functionality. 
  - Complex Condition within main methods.
  - Lazy Classes- Classes which is not worth of having in seperatly.
  - Large Classes. 
  - Variable or method without proper description of its purpose.
  - Primitive Obsession - a program can use too many primitive data types that should really be part of a class. 



## **When not to Refactor the code**
  <br/> As we know when to refactor the code, it is as important to know when to not Refactor the code.
  <br/>
  - When we find the code exhaust in period of time, there is no need of refactoring.
 - When code has lots of bug and need to be completly reframed its scratch.

## **Steps to Refactor code**
 1. Write the scratch code.
 2. Review code to identify refactorings.
 3. Apply only one refactoring at a time without changing functionality.
 4. Test the refactoring.
 5. Repeat to find more refactorings.

## Important point to be noted
  * General functionality should not be changed
  * Time and Memory efficency should be in consideration.
  * Refactoring of code should not affect the other part of code.

Now, lets go through the different way to refactor the code.
Look into brief introduction of [Refactoring](https://youtu.be/-lkiccO8h6w)


## Various way to Refactor the code

![Refactor cycle](https://media.geeksforgeeks.org/wp-content/cdn-uploads/20200922214720/Red-Green-Refactoring.png)

## **1. Refactoring By Abstraction**:
   
   This technique is mostly used by developers when there is a need to do a large amount of refactoring. Mainly we use this technique to reduce the redundancy (duplication) in our code. This involves class inheritances, hierarchy, creating new classes and interfaces, extraction, replacing inheritance with the delegation, and vice versa. 
  
   ### Points to be noted
 - Each code should have proper descriptive name.
 - Each Function should have only 4-10 line.
 - Each function should to only one task. Incase of Lazy function it can be combined with other function
 - Refactor the complex logic code into new function
<br/>
<br/>
Customer.java

 ```
    public class Customer{
          
        public ArrayList<Product> theProducts = new ArrayList<Product>();
        
        public void store(Product newProduct){
                
                theProducts.add(newProduct);
                
        }
        
        public void getCostOfProducts(){     
            for(Product product : theProducts){
                 System.out.println("Total cost for " + product.getQuantity() + " " + product.getName() + "s is $" + product.getTotalCost());
                        
                        System.out.println("Cost per product " + product.getTotalCost() / product.getQuantity());
                        
                        System.out.println("Savings per product " + ((product.getPrice() + product.getShippingCost()) - (product.getTotalCost() / product.getQuantity())) + "\n");     
                }
        
       }
      public void main(String arg[]){
        Customer cust=new Customer();
        cust.store(new Product("Rice",200,2));
        cust.store(new Product("Vegtable",20,1));  
        cust.store(new Product("Fruits",200,2));
      }
    }
 ```
 <br/>
 Pull-Up/Push-Down method is the best example of this approach. 
<br/>
Pull-Up method: It pulls code parts into a superclass and helps in the elimination of code duplication.
Push-Down method: It takes the code part from a superclass and moves it down into the subclasses.<br/>

In this example we are using Push-Down method to organize the data, process data and returning the required value as result to the main function.
<br/>
<br/>
 Product.java

 ```
   public class Product{
    int price=0, quantity=0;
    String item="";
    Product(String item,int price, int qty){
      this.item=item;
      this.price=price;
      this.quantity=qty;
    }

    public double getTotalCost(){
		
      if((quantity > 50) || ((quantity * price) > 500)) {
			
			quantityDiscount = .10;
			
		} else if((quantity > 25) || ((quantity * price) > 100)) {
			
			quantityDiscount = .07;
			
		} else if((quantity >= 10) || ((quantity * price) > 50)) {
			
			quantityDiscount = .05;
			
		  }
    }
    double discount = ((quantity - 1) * quantityDiscount) * price;
		
		return (quantity * price) + (quantity * shippingCost) - discount;
    }
   }
 ```
 **Note:** All other Code Refactoring is done using the above code.

## **2.Refactoring using Variable**:

- You can also use an explaining variable for complicated calculations. 
- It may however be better to extract this code into a method though to separate it from the method final is used to make sure the temp only has 1 value per iteration. It is bad practice to assign different values to a temp



Again considering the Product.java, we refactor to make it more easy to understand
<br/>
<br/>

Product.java

 ```
   public class Product{
    int price=0, quantity=0;
    String item="";
    Product(String item,int price, int qty){
      this.item=item;
      this.price=price;
      this.quantity=qty;
    }

    public double getTotalCost(){
		
       double quantityDiscount = 0.0;
       final boolean over50Products = (quantity > 50) || ((quantity * price) > 500);
       final boolean over25Products = (quantity > 25) || ((quantity * price) > 100);
       final boolean over10Products = (quantity >= 10) || ((quantity * price) > 50);
            
       if(over50Products) {
           quantityDiscount = .10;
        } else if(over25Products) {
           quantityDiscount = .07;
         } else if(over10Products) {
              quantityDiscount = .05;
        }
    
    double discount = ((quantity - 1) * quantityDiscount) * price;
		
		return (quantity * price) + (quantity * shippingCost) - discount;
   }
   }
 ```





 



























## **3.Inline Refactoring**:
Inline refactoring is a process of make a small function into an inline function

For example: 18+ are ready to vote if not cannot vote
```
if(age>18){
  System.out.println("You have vote right");
}
else{
  System.out.println("You don't have vote right");
}
```

This code can be refactored as
```
final String rightToVote=(age>18)?"You have vote right":"You don't have vote right";
System.out.println(rightToVote);
```





## **4.Extraction of Methods**:

We break the code into smaller chunks to find and extract fragmentation. After that, we create separate methods and then it is replaced with a call to this new method. 

```
public class SuperMarket{
    Scanner in=new Scanner(System.in);
    int mNum=0;
    String name=null;
    ArrayList<Integer> prod=new ArrayList<Integer>();
  public static void main(String arg[]){
   Scanner in=new Scanner(System.in);
    System.out.println("Enter the name:");
    name=in.nextLine();
    System.out.println("Enter the Mobile number:");
    mNum=in.nextInt();
   
    int i=1;
    while(i!=0){
    System.out.println("Enter the product( press 0 when CART is added): ");
    prod.add(in.next());
    }
  }
}
```

This is a complex method and has more number of line and more functionality. This can be fragmented into two : user details and product details

```
public class SuperMarket{
    Scanner in=new Scanner(System.in);
    int mNum=0;
    String name=null;
    ArrayList<Integer> prod=new ArrayList<Integer>();
  
  public static void main(String arg[]){
   userDetails();
   productDetails();
  }

  public void userDetails(){
    System.out.println("Enter the name:");
    name=in.nextLine();
    System.out.println("Enter the Mobile number:");
    mNum=in.nextInt();
  }

  public void productDetails(){
     int i=1;
    while(i!=0){
    System.out.println("Enter the product( press 0 when CART is added): ");
    prod.add(in.next());
   }
  }
}
```
This given each function its functionality and make the main code lean and easy to read. It may not show its usage now, but if upgarde product discount and other functionality to the program it means a lot.

## **5.Generalisation of method**:
Providing a generalize solution will allow us to do reusability of code and reduce the memory size.

 If we consider above example, we can have a generalize printing method to output the data. Even if the code is upgraded feature of discount, it can use same printing method to print the output.
    - Reduce the size of the code.
    - Increase the reusability of the code.

```
public void printing(item){
  for(String s:item){
    System.out.println("Item:"+s);
  }
}
```  
If we call printing with its respective parameter it will print details of different parameter. Thus a generalised class has been created. 

## **6.Refactor complex logic**:
When the logic statement of a program is big and not readable, then we can create a new method to solve that logic and make it simple and readable.

```
public class VoterID{
  ArrayList<voter> candidate=new  ArrayList<voter>();
    public void candidateDetails(voter info){
        candidate.add(info);
    }
    public void validate(){
        for(voter info:candidate){
            if(isEligible(info)){
                System.out.println(info.name+"can vote");
            }else{
                System.out.println(info.name+"can not vote");
            }
        }
    }
    public boolean isEligible(voter info){
        final boolean isValid=info.age>18 && info.nation;
        return isValid;
    }

    
    public static void main(String arg[]){
        VoterID info=new VoterID
        info.candidateDetails(new voter("Aswin",22,true));
        info.candidateDetails(new voter("Gokul",23,false));
        info.candidateDetails(new voter("Sham",17,true));
        info.validate();
    }

}

public class voter{
    String name;
    int age;
    boolean nation;
    voter(String name, int age, boolean nation){
        this.name=name;
        this.age=age;
        this.nation=nation
    }
}
```

In this, **If condition** will evaluate the condition by calling the function _isEligible_. In this case, it may be an easy evaluation but in the real-time project, it will be big to evaluate.

<br/>

## **When not to Refactor**:
If we consider the  example Product.java program from **Refactoring By Abstraction** topic, we can understand the where not to use Refactor.
<br/><br/>
Product.java

```
public class Product{
    int price=0, quantity=0;
    String item="";
    Product(String item,int price, int qty){
      this.item=item;
      this.price=price;
      this.quantity=qty;
    }

    public double getTotalCost(){
		
       double quantityDiscount = 0.0;
       final boolean over50Products = (quantity > 50) || ((quantity * price) > 500);
       final boolean over25Products = (quantity > 25) || ((quantity * price) > 100);
       final boolean over10Products = (quantity >= 10) || ((quantity * price) > 50);
            
       if(over50Products) {
           quantityDiscount = .10;
        } else if(over25Products) {
           quantityDiscount = .07;
         } else if(over10Products) {
              quantityDiscount = .05;
        }

//In this program I'm creating a new method to find discount, which is unwanted because it is a lazy method i.e, too many parameters have //to be passed for doing a single step and if doesn't have further use. 

      int TotalPrice=discount(quantity,quantityDiscount,price);
      return TotalPrice;

    //commented code is good way to use  
    //double discount = ((quantity - 1) * quantityDiscount) * price;
		//return (quantity * price) + (quantity * shippingCost) - discount;
    }

    public double discount(double quantity, float quantityDiscount, int price){
      double discount = ((quantity - 1) * quantityDiscount) * price;
      return (quantity*price)-discount;
    }

   }
 ```
In this code, creating _discount_ method has too many parameter for the simple calculation. Even it is more readable, the memory and time complexity will increase. <br/>
So, it is very important to decide where to refactor and where not to refactor the code.<br/>
To know more about refactoring in practical world click [Code Refactoring](https://youtu.be/vhYK3pDUijk)


## Reference Links:
1. Basics of Refactor: http://www.lcc.uma.es/~amg/ISE/OOP-Java-UML/Chapter8.html

2. When and How to Refactor: 
  - https://www.altexsoft.com/blog/engineering/code-refactoring-best-practices-when-and-when-not-to-do-it/ <br/><br/>
  -  https://rubygarage.org/blog/when-to-refactor-code

3. Methods to refactor: 
  - https://www.geeksforgeeks.org/7-code-refactoring-techniques-in-software-engineering/

  - https://en.wikipedia.org/wiki/Code_refactoring
  - https://refactoring.guru/refactoring/techniques

4. https://youtu.be/vhYK3pDUijk

5. https://youtu.be/-lkiccO8h6w




 
